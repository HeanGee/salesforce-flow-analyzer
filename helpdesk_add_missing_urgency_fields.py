import functools
import os
import xml.etree.ElementTree as ET
from typing import List

from sal_119 import (EMAIL_TARGETS, FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y,
                     create_tag, get_support_team_name, namespace,
                     recordCrete_inputAssignments_sorter, save, xml_printer)
from screen_builder import build_flow
from xml_tag_creator import *

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")


if __name__ == "__main__":

    flow_folder = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows'

    for flow_file in os.listdir(flow_folder):
        skip = True
        new_choice = False

        if any([forbidden in flow_file for forbidden in ['MainFlow.flow', 'Subflow_Email_Notification.flow']]):
            continue

        flow_to_update = os.path.join(flow_folder, flow_file)
        tree = ET.parse(flow_to_update)

        root = tree.getroot()
        ns = namespace(root)
        tree.find(f'./{ns}status').text = 'Draft'
        tree.find(f'./{ns}description').text = 'Add missing "Urgency" field.'

        # Check if Urgency Choice Set exists.
        urgency_choiceSet_name = ''

        existing_choiceSets: List[ET.Element] = root.findall(f'./{ns}dynamicChoiceSets')
        existing_choiceSets = list(filter(
            lambda cs: cs.find(f'{ns}picklistField').text == 'Urgency__c' and cs.find(f'{ns}picklistObject').text == 'Case',
            existing_choiceSets
        ))

        if len(existing_choiceSets) == 0:
            new_choiceSet = dynamicChoiceSets(data={'name': 'Urgency_ChoiceSet', 'picklistField': 'Urgency__c', 'picklistObject': 'Case'})
            FLOW_LAYOUT['dynamicChoiceSets'].append(new_choiceSet)
            urgency_choiceSet_name = 'Urgency_ChoiceSet'
        else:
            urgency_choiceSet_name = existing_choiceSets[0].find(f'{ns}name').text

        # Check if Urgency__c is being populated
        recordCreates: List[ET.Element] = root.findall(f'./{ns}recordCreates')

        for recordCreate in recordCreates:
            current_when = recordCreate.find(f'{ns}name').text.replace('CreateCase_when_', '')

            fields: List[ET.Element] = recordCreate.findall(f'./{ns}inputAssignments/{ns}field')
            urgency_field = list(filter(lambda field: field.text == 'Urgency__c', fields))

            if len(urgency_field) == 0:
                skip = False

                # Locate CaseDetails screen
                screen_name = f'CaseDetails_when_{current_when}'
                screens: List[ET.Element] = root.findall(f'./{ns}screens')
                screens = list(filter(lambda screen: screen.find(f'{ns}name').text == screen_name, screens))

                if len(screens) == 0:
                    raise NameError(f"Problem with {flow_file}: {screen_name} doesn't exists.")
                
                screen = screens[0]

                # Check if Urgency field is requested to user
                fields: List[ET.Element] = screen.findall(f'./{ns}fields')
                fields = list(filter(lambda field: 'urgency' in field.find(f'./{ns}name').text.lower(), fields))

                urgency_field_apiname = None

                if len(fields) == 0:
                    urgency_field_apiname = f'Field_Urgency_when_{current_when}'

                    urgency_field = field_DropdownBox(data={
                        'name': urgency_field_apiname,
                        'choiceReferences': urgency_choiceSet_name,
                        'dataType': 'String',
                        'stringValue': 'Medium',
                        'fieldText': 'The level of urgency of the request',
                        'isRequired': 'true',
                    })

                    # Insert into screen
                    child_pos = -1
                    inserted = False

                    for child in screen:
                        child_pos += 1

                        if 'field' in child.tag and not inserted:
                            screen.insert(child_pos+2, urgency_field)
                            inserted = True

                        if inserted:
                            break

                else:
                    urgency_field_apiname = fields[0].find(f'{ns}name').text


                # Populate Urgency__c field. Insert new inputAssignments element
                child_pos = -1
                inserted = False
                ia_pos = None

                inputAssignments = recordCreate.findall(f'./{ns}inputAssignments')
                
                for ia in inputAssignments:
                    recordCreate.remove(ia)

                new_assignment = recordCreate_inputAssignments(data={
                    'field': 'Urgency__c',
                    'elementReference': urgency_field_apiname,
                })

                inputAssignments.append(new_assignment)
                inputAssignments.sort(key=recordCrete_inputAssignments_sorter)
                pos = len(recordCreate)-2

                for i in range(len(inputAssignments)):
                    recordCreate.insert(pos+i, inputAssignments[i])
                    
        # # print()

        if not skip:
            # Build xml tree
            if len(FLOW_LAYOUT['dynamicChoiceSets']) > 0:
                tree = build_flow(FLOW_LAYOUT, root)
            save(tree, filename=flow_to_update)
        else:
            print(f'Skipping {flow_to_update}')
            print()

        FLOW_LAYOUT['dynamicChoiceSets'] = []
