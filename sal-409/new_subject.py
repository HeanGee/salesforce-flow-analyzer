import xml.etree.ElementTree as ET
import os
import yaml
import sys
from typing import List

try:
    # getting the name of the directory
    # where the this file is present.
    current = os.path.dirname(os.path.realpath(__file__))

    # Getting the parent directory name
    # where the current directory is present.
    parent = os.path.dirname(current)

    # adding the parent directory to
    # the sys.path.
    sys.path.append(parent)

    from sal_119 import FLOW_LAYOUT, build_regions, create_tag, namespace, save, xml_printer
    from screen_builder import build_flow, fill_flow
except Exception as exc:
    raise exc

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")


def getSubflowInputAssignmentForEmailSubject(subflow: ET.ElementTree, ns: str):
    inputAssignments = subflow.findall(f'./{ns}inputAssignments')
    emailsubjectnode = list(filter(lambda ia: ia.find(f'./{ns}name').text == 'In_EmailSubject', inputAssignments))[0]
    value = emailsubjectnode.find(f'./{ns}value/{ns}stringValue')

    return value

def getFlows(flowfolder: str):
    flowfilter = ['CIE', 'Request', 'Facilities', 'Resource', 'Salesforce', 'Technology']
    # flowfilter = ['Technology']
    flows = []

    for flowfile in os.listdir(flowfolder):
        if (not any([ffilter in flowfile for ffilter in flowfilter])): continue

        flows.append((ET.parse(os.path.join(flowfolder, flowfile)), flowfile))

    return flows

def updateDescription(flow: ET.ElementTree, description: str):
    ns = namespace(flow.getroot())
    flow.find(f'./{ns}description').text = description
    flow.find(f'./{ns}status').text = 'Active'

def getFieldNameByPattern(flow: ET.ElementTree, flowname: str, fieldnamepattern: str):
    ns = namespace(flow.getroot())
    screenFields = flow.findall(f'./{ns}screens/{ns}fields')
    fieldnamepattern = fieldnamepattern.lower()
    located_field = None

    for field in screenFields:
        name = field.find(f'./{ns}name').text
        lowered = name.lower()

        # print(f"comparing against '{name}': {fieldnamepattern in lowered}")

        located_field = name if fieldnamepattern in lowered else located_field
    
    if located_field is None:
        raise ValueError(f"The script couldn't found any '{fieldnamepattern}' field from {flowname}")
    
    return located_field


if __name__ == "__main__":
    flowfolder = '/Users/daniel.m/Git/211-san-diego/force-app/main/default/flows'
    nameprefx = 'Subflow_EmailNotif'

    for flow, flowfile in getFlows(flowfolder):
        ns = namespace(flow.getroot())

        updateDescription(flow, 'SAL-409: Update Email Subject, Recipients and Body')

        subflows = flow.findall(f'./{ns}subflows')
        processed_nodes = 0
        print(f"=====================\nFlow {flowfile}\n=====================\n")
        print(f"Subflows count: {len(subflows)}")

        for subflow in subflows:
            namesufx = subflow.find(f'./{ns}name').text.replace(nameprefx, '')
            subjectfield = getFieldNameByPattern(flow, flowfile, f'subject{namesufx}')
            urgencyfield = getFieldNameByPattern(flow, flowfile, f'urgency{namesufx}')
            
            value = getSubflowInputAssignmentForEmailSubject(subflow, ns)
            value.text = "New HelpDesk Request: Subject - {!%s} - {!%s}"%(subjectfield, urgencyfield)
            processed_nodes += 1

        print(f"Processed nodes: {processed_nodes}")
        save(flow, os.path.join(flowfolder, flowfile))
        # save(flow, os.path.join(flowfolder, '.2.xml'))
        print()


    print("\nDONE\n")