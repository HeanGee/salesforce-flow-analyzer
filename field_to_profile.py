import json
import os
import re
import xml
import xml.etree.ElementTree as ET
from venv import create

import yaml

from sal_119 import (FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y, create_tag, EMAIL_TARGETS,
                     get_support_team_name, namespace, save, xml_printer)
from screen_builder import build_flow

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")



if __name__ == "__main__":

    APEX = [
        'ConsentController',
        'ConsentControllerTest',
        'DigitalSignatureWrapper',
        'PartnerCommunityClientController',
        'SaveConsentPDFQueueable',
        'SaveConsentPDFQueueableTest'
    ]
    FIELDS = [
        'Client_Search_History__c.Medi_Cal_CIN__c',
        'Privacy__c.Completed_by_parent_or_legal_guardian__c',
        'Privacy__c.Digital_Signature_Confirmation__c',
        'Privacy__c.Minor_can_sign_the_release__c',
        'Privacy__c.Minor_is_completing_own_authorization__c',
        'Privacy__c.Status__c',
        'Privacy__c.You_are_acting_as_a_health_provider__c'
    ]

    package = ET.parse('/Users/daniel.m/git-repos/211-san-diego/manifest/package.xml')
    root = package.getroot()
    ns = namespace(root)
    members = package.findall(f'./{ns}types/{ns}members')
    members = [member.text for member in members]

    profile_folder = '/Users/daniel.m/git-repos/211-san-diego/force-app/main/default/profiles'

    for profile_file in os.listdir(profile_folder):

        if not(any([re.match(member, profile_file) for member in members])):
            continue

        profile = os.path.join(profile_folder, profile_file)
        tree = ET.parse(profile)

        root = tree.getroot()
        ns = namespace(root)
        
        for apex in APEX:
            classAccesses = ET.Element('classAccesses')
            apexClass = ET.Element('apexClass')
            enabled = ET.Element('enabled')

            enabled.text = 'true'
            apexClass.text = apex

            classAccesses.append(apexClass)
            classAccesses.append(enabled)

            root.append(classAccesses)
        
        for field in FIELDS:
            fieldPermissions = ET.Element('fieldPermissions')
            editable = ET.Element('editable')
            _field = ET.Element('field')
            readable = ET.Element('readable')

            editable.text = 'true'
            _field.text = field
            readable.text = 'true'

            fieldPermissions.append(editable)
            fieldPermissions.append(_field)
            fieldPermissions.append(readable)

            root.append(fieldPermissions)


        # xml_printer(root)
        # tree.find(f'./{ns}status').text = 'Active'
        # tree.find(f'./{ns}description').text = 'Update Email Subject'

        # subflows = tree.findall(f'./{ns}subflows')

        # for subflow in subflows:
        #     current_when = subflow.find(f'./{ns}name').text.replace('Subflow_EmailNotif_when_', '')
        #     new_subflow_input_varname = 'In_EmailBodySection'

        #     if any([name.text == new_subflow_input_varname for name in subflow.findall(f'./{ns}inputAssignments/{ns}name')]):
        #         continue

        #     inputAssignments = ET.Element('inputAssignments')
        #     name = ET.Element('name')
        #     value = ET.Element('value')
        #     stringValue = ET.Element('stringValue')

        #     name.text = 'In_EmailBodySection'
        #     stringValue.text = '\'{!In_InitialRequestArea}\' issue regarding \'{!In_Lv1}\''

        #     value.append(stringValue)
        #     inputAssignments.append(name)
        #     inputAssignments.append(value)

        #     subflow.append(inputAssignments)
        
        # for subflow in subflows:
        #     support_team = get_support_team_name(tree, 'CreateCase_when_' + current_when)

        #     inputAssignments = subflow.findall(f'./{ns}inputAssignments')
        #     inputAssignment = list(filter(lambda element: element.find(f'./{ns}name').text == 'In_Sender_EmailAddress', inputAssignments))[0]
        #     inputAssignment.find(f'./{ns}value/{ns}stringValue').text = EMAIL_TARGETS[support_team].strip()

        #     recordCreateName = "CreateCase_when_" + current_when
        #     inputAssignments = tree.findall(f'./{ns}recordCreates/{ns}inputAssignments')
        #     inputAssignments = list(filter(lambda element: element.find(f'./{ns}field').text == 'Support_Team__c', inputAssignments))
        #     for ia in inputAssignments:
        #         teams.add(ia.find(f'./{ns}value/{ns}stringValue').text)
        
        # # Build xml tree
        # tree = build_flow(FLOW_LAYOUT, root)
        # root = tree.getroot()
        # ns = namespace(root)

        save(tree, filename=profile)

    # for team in teams:
    #     print(team)