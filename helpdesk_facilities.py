import xml.etree.ElementTree as ET

import yaml

from sal_119 import (FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y, create_tag, namespace, save)
from screen_builder import build_flow

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")


if __name__ == "__main__":

    recipe = yaml.safe_load(open("input/helpdesk_facilities_recipe.yaml", 'r'))
    # import json as js
    # print(js.dumps(recipe, indent=2))

    if 'recordCreates' in recipe.keys():
        # Preprocess recordCreates fields
        for recordCreate in recipe['recordCreates']:
            for inputAssignments in recordCreate['inputAssignments']:
                if 'elementReference' in inputAssignments.keys():
                    is_user_input_field = 'Field_' in inputAssignments['elementReference']

                    if is_user_input_field:
                        inputAssignments['elementReference'] = '_'.join([inputAssignments['elementReference'], 'when', recordCreate['name'].replace('CreateCase_when_', '')])
                        inputAssignments['elementReference'] += '.recordId' if 'isIDField' in inputAssignments.keys() and inputAssignments['isIDField'] == True else ''
                        inputAssignments['elementReference'] += '.value' if 'isEmailField' in inputAssignments.keys() and inputAssignments['isEmailField'] == True else ''

    if 'screens' in recipe.keys():
        # Preprocess Finalization screens fields
        for screen in recipe['screens']:
            is_final_screen = 'FinalizationScreen_' in screen['name']
            is_createcase_screen = 'CaseDetails_when_' in screen['name']

            # if is_createcase_screen:
            #     for field in screen['fields']:
            #         field['name'] = '_'.join([field['name'], 'when', screen['name'].replace('CaseDetails_when_', '')])
            
            if is_final_screen:
                for field in screen['fields']:
                    field['name'] = '_'.join([field['name'], 'when', screen['name'].replace('FinalizationScreen_when_', '')])
        

    start_locationX = recipe['start.locationX']
    start_locationY = recipe['start.locationY']

    flow = create_tag(type='base_skeleton', data={
        'label': recipe['label'],
        'start.locationX': str(recipe['start.locationX']),
        'start.locationY': str(recipe['start.locationY']),
        'start.targetReference': recipe['start.targetReference'],
        'description': recipe['description'],
    })
    # xml_printer(flow)
    # print()

    for i in range(len(recipe['decisions'])):
        decisions = create_tag(type='decisions', data={
            'label': recipe['decisions'][i]['label'],
            'name': recipe['decisions'][i]['name'],
            'rules': [
                create_tag(type='decision_rules', data={
                    'name': rule['name'],
                    'label': rule['label'],
                    'conditionLogic': rule['conditionLogic'],
                    'leftValueReference': rule['leftValueReference'],
                    'operator': rule['operator'],
                    'stringValue': rule['rightStringValue'],
                    'targetReference_c': rule['targetReference'] if 'targetReference' in rule.keys() else None
                })
                for rule in recipe['decisions'][i]['rules']
            ],
            'defaultConnectorLabel': recipe['decisions'][i]['defaultConnectorLabel'],
            'locationX': str(start_locationX),
            'locationY': str(start_locationY+INCREMENT_Y),
        })
        FLOW_LAYOUT['decisions'].append(decisions)

    if 'screens' in recipe.keys():
        case_details_screens = []
        for i in range(len(recipe['screens'])):
            fields = []
            for field in recipe['screens'][i]['fields']:
                data = {
                    "name": '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]),
                    "dataType": field['dataType'],
                    "fieldText": field['fieldText'],
                    "fieldType": field['fieldType'],
                    "isRequired": field['isRequired'],
                } if field['fieldType'] == 'InputField' and 'choiceReferences' not in field.keys() else (

                    {
                        "name": '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]),
                        "fieldText": field['fieldText'],
                        "fieldType": field['fieldType'],
                        "isRequired": field['isRequired'],
                    }
                    if field['fieldType'] == "LargeTextArea" else

                    {
                        "name": '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]),
                        "dataType": field['dataType'],
                        "fieldText": field['fieldText'],
                        "fieldType": field['fieldType'],
                        "isRequired": field['isRequired'],
                        "choiceReferences": field['choiceReferences']
                    }
                    if field['fieldType'] in ['MultiSelectPicklist', 'DropdownBox'] and 'choiceReferences' in field.keys() else
                    
                    {
                        'name': '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]),
                        'extensionName': field['extensionName'],
                        'fieldType': field['fieldType'],
                        'inputsOnNextNavToAssocScrn': field['inputsOnNextNavToAssocScrn'],
                        'storeOutputAutomatically': field['storeOutputAutomatically'],
                        'isRequired': field['isRequired'],
                        'inputParameters': field['inputParameters'],
                    }
                    if 'extensionName' in field.keys() and field['extensionName'] == 'flowruntime:lookup' else
                    
                    {
                        "name": field['name'],
                        "fieldType": field['fieldType'],
                        "isRequired": field['isRequired'],
                        "extensionName": field['extensionName'],
                        "inputsOnNextNavToAssocScrn": field['inputsOnNextNavToAssocScrn'],
                        "storeOutputAutomatically": field['storeOutputAutomatically'],
                        "inputParameters": field['inputParameters'],
                    }
                    if 'extensionName' in field.keys() and field['extensionName'] == 'forceContent:fileUpload' else

                    {
                        "name": '_'.join([field['name'], 'when', recipe['screens'][i]['name'].replace('CaseDetails_when_', '')]),
                        "fieldType": field['fieldType'],
                        "isRequired": field['isRequired'],
                        "extensionName": field['extensionName'],
                        "inputsOnNextNavToAssocScrn": field['inputsOnNextNavToAssocScrn'],
                        "storeOutputAutomatically": field['storeOutputAutomatically'],
                        "inputParameters": field['inputParameters'],
                    }
                    if 'extensionName' in field.keys() and field['extensionName'] == 'flowruntime:email' else

                    {
                        'name': field['name'],
                        'fieldType': field['fieldType'],
                        'fieldText': field['fieldText'],
                    }
                    if field['fieldType'] == 'DisplayText' else None
                )

                if data is None:
                    print(field)
                    print()

                fields.append(create_tag(type='screen_field', data=data))
            
            screen_data = {
                'name': recipe['screens'][i]['name'],
                'label': recipe['screens'][i]['label'],
                'allowBack': recipe['screens'][i]['allowBack'],
                'allowFinish': recipe['screens'][i]['allowFinish'],
                'allowPause': recipe['screens'][i]['allowPause'],
                'showFooter': recipe['screens'][i]['showFooter'],
                'showHeader': recipe['screens'][i]['showHeader'],
                'fields': fields,
                'x': str(start_locationX+INCREMENT_X*i),
                'y': str(start_locationY+INCREMENT_Y*2),
            }

            if 'targetReference' in recipe['screens'][i].keys():
                screen_data['targetReference'] = recipe['screens'][i]['targetReference']

            screen = create_tag(type='screens', data=screen_data)
            case_details_screens.append(screen)

        FLOW_LAYOUT['screens'] = case_details_screens

    if 'dynamicChoiceSets' in recipe.keys():
        FLOW_LAYOUT['dynamicChoiceSets'] = [
            create_tag(type='dynamicChoiceSets', data={
                "name": dcs['name'],
                "dataType": dcs['dataType'],
                "displayField-xsi:nil": dcs['displayField-xsi:nil'],
                "object-xsi:nil": dcs['object-xsi:nil'],
                "picklistField": dcs['picklistField'],
                "picklistObject": dcs['picklistObject'],
            })
            for dcs in recipe['dynamicChoiceSets']
        ]

    FLOW_LAYOUT['variables'] = [
        create_tag(type='variables', data={
            'name': variable['name'],
            'dataType': variable['dataType'],
            'isCollection': variable['isCollection'],
            'isInput': variable['isInput'],
            'isOutput': variable['isOutput'],
        })
        if variable['dataType'] == 'String' else

        create_tag(type='variables', data={
            'name': variable['name'],
            'dataType': variable['dataType'],
            'isCollection': variable['isCollection'],
            'isInput': variable['isInput'],
            'isOutput': variable['isOutput'],
            'objectType': variable['objectType']
        })
        if variable['dataType'] == 'SObject' else None

        for variable in recipe['variables']
    ]

    if 'recordCreates' in recipe.keys():
        recordCreates = []
        for recordCreate in recipe['recordCreates']:
            rc_data = {
                'name': recordCreate['name'],
                'label': recordCreate['label'],
                'object': recordCreate['object'],
                'inputAssignments': recordCreate['inputAssignments'],
                'storeOutputAutomatically': recordCreate['storeOutputAutomatically'],
                'x': start_locationX,
                'y': start_locationY,
            }

            if 'targetReference_c' in recordCreate.keys():
                rc_data['targetReference_c'] = '_'.join([recordCreate['targetReference_c'], 'when', recordCreate['name'].replace('CreateCase_when_', '')])
            if 'targetReference_fc' in recordCreate.keys():
                rc_data['targetReference_fc'] = recordCreate['targetReference_fc']

            recordCreates.append(create_tag(type='recordCreates', data=rc_data))
        FLOW_LAYOUT['recordCreates'] = recordCreates

    # Build xml tree
    tree = build_flow(FLOW_LAYOUT, flow)
    root = tree.getroot()
    ns = namespace(root)

    # save(tree, filename="new_flow.xml")
    save(tree, filename="D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows/HelpDesk_Facilities.flow-meta.xml")