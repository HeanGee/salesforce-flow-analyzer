import os
import sys
import xml.etree.ElementTree as ET
from copy import deepcopy
from doctest import debug


try:
    # getting the name of the directory
    # where the this file is present.
    current = os.path.dirname(os.path.realpath(__file__))

    # Getting the parent directory name
    # where the current directory is present.
    parent = os.path.dirname(current)

    # adding the parent directory to
    # the sys.path.
    sys.path.append(parent)

    from sal_119 import (FLOW_LAYOUT, build_regions, create_tag, namespace,
                         save, xml_printer)
    from screen_builder import build_flow, fill_flow
except Exception as exc:
    raise exc


ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")



if __name__ == '__main__':
    flowfolder = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows'
    flowsToExclude = ['HelpDesk_Email_when_Case_closed.flow-meta', 'HelpDesk_MainFlow.flow-meta', ]
    
    for flowfile in os.listdir(flowfolder):
        if not ('HelpDesk' in flowfile and all([toExclude not in flowfile for toExclude in flowsToExclude])):
            continue

        flow = ET.parse(os.path.join(flowfolder, flowfile))
        root = flow.getroot()
        ns = namespace(root)

        root.find(f'./{ns}description').text = 'SAL-529: Remove OLD_ from Level X field API names.'
        root.find(f'./{ns}status').text = 'Active'

        save(flow, os.path.join(flowfolder, flowfile))

    print('DONE\n\n')