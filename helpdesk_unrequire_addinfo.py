import functools
import os
import xml.etree.ElementTree as ET
from typing import List

from sal_119 import (EMAIL_TARGETS, FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y,
                     create_tag, get_support_team_name, namespace,
                     recordCrete_inputAssignments_sorter, save, xml_printer)
from screen_builder import build_flow
from xml_tag_creator import *

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")


if __name__ == "__main__":

    flow_folder = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows'

    for flow_file in os.listdir(flow_folder):
        skip = True
        new_choice = False
        modified = False

        if any([forbidden in flow_file for forbidden in ['MainFlow.flow', 'Subflow_Email_Notification.flow']]):
            continue

        flow_to_update = os.path.join(flow_folder, flow_file)
        tree = ET.parse(flow_to_update)

        root = tree.getroot()
        ns = namespace(root)
        tree.find(f'./{ns}status').text = 'Active'
        tree.find(f'./{ns}description').text = 'Un-Require Additional Info field input.'

        for screentype in ['upload', 'finalization']:
            screens: List[ET.Element] = root.findall(f'./{ns}screens')
            screens = list(filter(lambda screen: 'casedetail' in screen.find(f'./{ns}name').text.lower(), screens))

            fields: List[ET.Element] = root.findall(f'{ns}screens/{ns}fields')
            fields = list(filter(lambda field: 'additionalinfo' in field.find(f'./{ns}name').text.lower(), fields))
            fields = list(filter(lambda field: field.find(f'./{ns}isRequired').text.lower() == 'true', fields))

            def assign(tag, value):
                tag.text = value

            for field in fields:
                assign(field.find(f'{ns}isRequired'), 'false')
                modified = True

            # print(f"For {flow_file}: We have {len(screens)} branches")
        # print()

        save(tree, filename=flow_to_update) if modified else None

