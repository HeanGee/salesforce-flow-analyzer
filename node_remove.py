import xml.etree.ElementTree as ET
from sal_119 import namespace, xml_printer, save

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")

input_file = "/Users/daniel.m/Git/211-san-diego/force-app/main/default/flows/HelpDesk_MainFlow.flow-meta.xml"
output_file = "/Users/daniel.m/Git/211-san-diego/force-app/main/default/flows/HelpDesk_MainFlow.flow-meta.xml"
tree = ET.parse(input_file)
root = tree.getroot()
ns = namespace(root)

todelete_nodenames = ["Decision_Level1Category_when_IRAIs_DataRequest"]

for todelete_nodename in todelete_nodenames:
    parent_node = tree.find(f".//{ns}connector[{ns}targetReference='{todelete_nodename}']/..")
    if parent_node is None:
        print(f"No node has been found as parent of '{todelete_nodename}'.")
        continue

    # Unlink parent node from node to delete
    parent_node_connector = parent_node.find(f"./{ns}connector")
    parent_node.remove(parent_node_connector)

    # Delete all nodes below the node to delete.
    nodes_todelete = []
    nodes_todelete.append(
        tree.find(f"./*[{ns}name='{todelete_nodename}']")  # Collect the first node to delete, just right below the parent.
    )
    fault_screens = []
    processed_fault_screen_names = set()

    # Each node to delete can have a child that should also be deleted
    for node_todelete in nodes_todelete:
        typeof_node_todelete = node_todelete.tag

        if typeof_node_todelete == f"{ns}decisions":
            # Default branch can have or not a screen
            default_outcome_node = node_todelete.find(f"{ns}defaultConnector")
            if default_outcome_node is not None:
                next_node_todelete_name = default_outcome_node.find(f"{ns}targetReference").text
                next_node_todelete = tree.find(f"./*[{ns}name='{next_node_todelete_name}']")
                nodes_todelete.append(next_node_todelete)
            
            # Iterate over rules.
            # Each rule has a connector to a child tree
            for rule in node_todelete.findall(f"{ns}rules"):
                connector = rule.find(f"./{ns}connector/{ns}targetReference")
                next_node_todelete_name = connector.text
                next_node_todelete = tree.find(f"./*[{ns}name='{next_node_todelete_name}']")
                nodes_todelete.append(next_node_todelete)

        elif typeof_node_todelete in [f"{ns}screens", f"{ns}recordCreates", f"{ns}recordLookups"]:
            connector = node_todelete.find(f"./{ns}connector/{ns}targetReference")
            if connector is not None:
                next_node_todelete_name = connector.text
                next_node_todelete = tree.find(f"./*[{ns}name='{next_node_todelete_name}']")
                nodes_todelete.append(next_node_todelete)

            # Fault screens
            fault_screen = node_todelete.find(f"./{ns}faultConnector/{ns}targetReference")
            if fault_screen is not None:
                next_node_todelete_name = fault_screen.text
                if next_node_todelete_name not in processed_fault_screen_names:
                    next_node_todelete = tree.find(f"./*[{ns}name='{next_node_todelete_name}']")
                    fault_screens.append(next_node_todelete)
                    processed_fault_screen_names.add(next_node_todelete_name)

    # Remove collected nodes from tree
    _ = [ root.remove(element) for element in nodes_todelete ]

    # Verify that any of the remaining nodes make use of any of the collected fault_screen.
    # If is not being used, delete, otherwise, keep.
    for fs in fault_screens:
        name = fs.find(f"{ns}name").text
        record_creates = [element for element in tree.iter() if element.tag == f"{ns}recordCreates" and element.find(f"./{ns}faultConnector/{ns}targetReference") is not None]
        rc_size = len(record_creates)
        if rc_size > 0:
            isGoTos = [
                rc.find(f"./{ns}faultConnector/{ns}targetReference").text == name and \
                    rc.find(f"./{ns}faultConnector/{ns}isGoTo") is not None and \
                        rc.find(f"./{ns}faultConnector/{ns}isGoTo").text == "true"
                for rc in record_creates
            ]
            if all(isGoTos):
                # Make at least one not an "isGoTo": Remove <isGoTo> element
                record_create = record_creates[0]
                isGoTo = record_create.find(f"./{ns}faultConnector/{ns}isGoTo")
                record_create.find(f"./{ns}faultConnector").remove(isGoTo)
            else:
                is_no_longer_used = [rc.find(f"./{ns}faultConnector/{ns}targetReference").text != name for rc in record_creates]
                if all(is_no_longer_used):
                    # the fault screen is not being used anymore by any node. So it can be deleted
                    root.remove(fs)


    # Save the new xml
    save(tree, filename=output_file)