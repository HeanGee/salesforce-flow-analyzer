import json
import os
import xml
import xml.etree.ElementTree as ET
from venv import create

import yaml

from sal_119 import (FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y, create_tag, EMAIL_TARGETS,
                     get_support_team_name, namespace, save, xml_printer)
from screen_builder import build_flow

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")



if __name__ == "__main__":

    flow_folder = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows'
    teams = set()
    for flow_file in os.listdir(flow_folder):
        if any([forbidden in flow_file for forbidden in ['MainFlow.flow', 'Subflow_Email_Notification.flow']]):
            continue

        flow_to_update = os.path.join(flow_folder, flow_file)
        tree = ET.parse(flow_to_update)

        root = tree.getroot()
        ns = namespace(root)
        tree.find(f'./{ns}status').text = 'Active'
        tree.find(f'./{ns}description').text = 'Update Email Subject'

        subflows = tree.findall(f'./{ns}subflows')

        for subflow in subflows:
            current_when = subflow.find(f'./{ns}name').text.replace('Subflow_EmailNotif_when_', '')
            new_subflow_input_varname = 'In_EmailBodySection'

            if any([name.text == new_subflow_input_varname for name in subflow.findall(f'./{ns}inputAssignments/{ns}name')]):
                continue

            inputAssignments = ET.Element('inputAssignments')
            name = ET.Element('name')
            value = ET.Element('value')
            stringValue = ET.Element('stringValue')

            name.text = 'In_EmailBodySection'
            stringValue.text = '\'{!In_InitialRequestArea}\' issue regarding \'{!In_Lv1}\''

            value.append(stringValue)
            inputAssignments.append(name)
            inputAssignments.append(value)

            subflow.append(inputAssignments)
        
        for subflow in subflows:
            support_team = get_support_team_name(tree, 'CreateCase_when_' + current_when)

            inputAssignments = subflow.findall(f'./{ns}inputAssignments')
            inputAssignment = list(filter(lambda element: element.find(f'./{ns}name').text == 'In_Sender_EmailAddress', inputAssignments))[0]
            inputAssignment.find(f'./{ns}value/{ns}stringValue').text = EMAIL_TARGETS[support_team].strip()

            recordCreateName = "CreateCase_when_" + current_when
            inputAssignments = tree.findall(f'./{ns}recordCreates/{ns}inputAssignments')
            inputAssignments = list(filter(lambda element: element.find(f'./{ns}field').text == 'Support_Team__c', inputAssignments))
            for ia in inputAssignments:
                teams.add(ia.find(f'./{ns}value/{ns}stringValue').text)
        
        # Build xml tree
        tree = build_flow(FLOW_LAYOUT, root)
        root = tree.getroot()
        ns = namespace(root)

        save(tree, filename=flow_to_update)

    # for team in teams:
    #     print(team)