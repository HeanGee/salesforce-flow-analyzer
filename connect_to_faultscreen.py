import xml.etree.ElementTree as ET
from sal_119 import namespace, save, create_tag, xml_printer


if __name__ == "__main__":
    ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")

    tree = ET.parse("output/HelpDesk_MainFlow.flow-meta.xml")
    root = tree.getroot()
    ns = namespace(root)

    faultConnectorTag = create_tag(type="faultConnector", data={
        "isGoTo": True,
        "targetReference": "Screen_ExceptionDisplayer"
    })
    xml_printer(faultConnectorTag)

    for recordCreates in tree.findall(f"{ns}recordCreates"):
        if not recordCreates.find(f"{ns}faultConnector"):
            child_idx = -1
            for child in recordCreates:
                child_idx += 1
                if child.tag == f"{ns}connector":
                    recordCreates.insert(child_idx+1, faultConnectorTag)

    save(tree)

