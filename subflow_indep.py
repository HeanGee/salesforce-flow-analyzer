import xml.etree.ElementTree as ET
from sal_119 import namespace, save, create_tag, xml_printer
from screen_builder import general_service_feedback


if __name__ == "__main__":
    ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")

    xml_filename = "HelpDesk_Resource_Center.flow-meta.xml"
    tree = ET.parse(xml_filename)
    root = tree.getroot()
    ns = namespace(root)

    # Create Input variables
    variable_names = (
        "In_RecordType_CIESanDiego",
        "In_RecordType_ResourceDatabase",
        "In_Queue_CIELocal",
        "In_Queue_Resource",
        "In_InitialRequestArea",
        "In_Lv1",
        "In_Lv2",
    )

    for variable in variable_names:
        if variable in ["In_Lv1", "In_Lv2", "In_InitialRequestArea"]:
            data={
                "name": variable,
                "dataType": "String",
                "isCollection": "false",
                "isInput": "true",
                "isOutput": "false",
            }
        else:
            data={
                "name": variable,
                "dataType": "SObject",
                "isCollection": "false",
                "isInput": "true",
                "isOutput": "false",
                "objectType": "RecordType" if "RecordType" in variable else ("Group" if "Queue" in variable else "")
            }
        root.append(create_tag("input_variable", data=data))

    faultConnectorTag = create_tag(type="faultConnector", data={
        "isGoTo": True,
        "targetReference": "Screen_ExceptionDisplayer"
    })

    # Define choices
    # Create Decision and Rules
    decisions = tree.find(f"{ns}decisions")
    decisions.find(f"{ns}name").text = "Decision_Lv1"
    decisions.find(f"{ns}label").text = "If the Level 1 Category is"
    choice_names = {
        "Level1Cat_GralSrvQuestionOrFeedback_Choice": "General Service Question or Feedback",
        "Level1Cat_PartnerRequestingCallBack_Choice": "Partner Requesting Call Back",
        "Level1Cat_RecommendedAgencyOrService_Choice": "Recommended an Agency or Service",
        "Level1Cat_ServiceEditOrFeedback_Choice": "Service Edit or Feedback",
    }
    method_name = {
        "Level1Cat_GralSrvQuestionOrFeedback_Choice": "general_service_feedback",
        "Level1Cat_PartnerRequestingCallBack_Choice": "general_service_feedback",
        "Level1Cat_RecommendedAgencyOrService_Choice": "general_service_feedback",
        "Level1Cat_ServiceEditOrFeedback_Choice": "general_service_feedback",
    }
    child_idx = -1
    first_branch_completed = False
    caseDetails_screens = []
    recordCreates_screens = []
    fileUpload_screens = []
    fault_screens = []
    rowx = 10
    colx = 10
    incx = 264
    incy = 216
    for child in root:
        child_idx += 1
        if child.tag == f"{ns}apiVersion":
            for choice_apiname, label_and_value in choice_names.items():
                globals()[method_name[choice_apiname]](root, ns, child_idx, label_and_value, choice_apiname, decisions, colx, rowx, incx, incy, first_branch_completed)
                # # Insert new Lv1 choice
                # root.insert(child_idx+1, create_tag(type="choices", data={
                #     "typeValueTag": "stringValue",
                #     "typeValueText": label_and_value,
                #     "dataType": "String",
                #     "choiceText": label_and_value,
                #     "name": choice_apiname
                # }))
                
                # # Insert new rule on Lv1 choice
                # decisions.append(
                #     create_tag(type="decision_rules", data={
                #         "name": label_and_value.replace(" ", "_"),
                #         "conditionLogic": "and",
                #         "leftValueReference": "In_Lv1",
                #         "operator": "EqualTo",
                #         "rightValueElementReference": choice_apiname,
                #         "label": label_and_value,
                #         "targetReference_c": f"CaseDetails_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}"
                #     })
                # )

                # # Create screen fields
                # account_name_field = create_tag(type="screen_field", data={
                #     "name": f"Field_AccountName_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "fieldType": "ComponentInstance",
                #     "isRequired": "true",
                #     "extensionName": "flowruntime:lookup",
                #     "inputsOnNextNavToAssocScrn": "UseStoredValues",
                #     "storeOutputAutomatically": "true",
                #     "inputParameters": [
                #         {
                #             "name": "fieldApiName",
                #             "typeValueTag": "stringValue",
                #             "typeValueText": "AccountId",
                #         },
                #         {
                #             "name": "label",
                #             "typeValueTag": "stringValue",
                #             "typeValueText": "What is the name of the agency?",
                #         },
                #         {
                #             "name": "objectApiName",
                #             "typeValueTag": "stringValue",
                #             "typeValueText": "Case",
                #         },
                #         {
                #             "name": "required",
                #             "typeValueTag": "booleanValue",
                #             "typeValueText": "true",
                #         },
                #     ]
                # })
                # subject_field = create_tag(type="screen_field", data={
                #     "name": f"Field_Subject_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "dataType": "String",
                #     "fieldText": "Subject",
                #     "fieldType": "InputField",
                #     "isRequired": "true",
                # })
                # descrip_field = create_tag(type="screen_field", data={
                #     "name": f"Field_Description_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "fieldText": "Description",
                #     "fieldType": "LargeTextArea",
                #     "isRequired": "true",
                # })
                # srvname_field = create_tag(type="screen_field", data={
                #     "name": f"Field_ServiceName_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "dataType": "String",
                #     "fieldText": "What is the name of the service?",
                #     "fieldType": "InputField",
                #     "isRequired": "true",
                # })
                # addinfo_field = create_tag(type="screen_field", data={
                #     "name": f"Field_AdditionalInfo_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "fieldText": "Any miscellaneous information that may be relevant to the request that was not adequately covered by other fields",
                #     "fieldType": "LargeTextArea",
                #     "isRequired": "false",
                # })
                # casedet_screen = create_tag(type="screens", data={
                #     "name": f"CaseDetails_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "label": "Case Details",
                #     "allowBack": "true",
                #     "allowFinish": "true",
                #     "allowPause": "true",
                #     "targetReference": f"CreateCase_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "showFooter": "true",
                #     "showHeader": "true",
                #     "x": str(colx),
                #     "y": str(rowx),
                #     "fields": [
                #         account_name_field, subject_field, descrip_field, srvname_field, addinfo_field
                #     ]
                # })

                # fault_screen = create_tag(type="fault_screen", data={
                #     "x": str(colx+incx),
                #     "y": str(rowx+incy)
                # }) if not first_branch_completed else None

                # createR_screen = create_tag(type="recordCreates", data={
                #     "name": f"CreateCase_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "label": "Create Case",
                #     "object": "Case",
                #     "storeOutputAutomatically": "true",
                #     "targetReference_c": f"FileUpload_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "targetReference_fc": "Fault_Screen",
                #     "isGoTo_fc": "true" if first_branch_completed else "false",
                #     "x": str(colx),
                #     "y": str(rowx+incy),
                #     "inputAssignments": [
                #         {
                #             "field": "AccountId",
                #             "elementReference": f"{account_name_field.find(f'name').text}.recordId"
                #         },
                #         {
                #             "field": "Additional_Information__c",
                #             "elementReference": subject_field.find(f"name").text
                #         },
                #         {
                #             "field": "Description",
                #             "elementReference": descrip_field.find(f"name").text
                #         },
                #         {
                #             "field": "Initial_Request_Area__c",
                #             "elementReference": "In_InitialRequestArea"
                #         },
                #         {
                #             "field": "Level_1_Category__c",
                #             "elementReference": "In_Lv1"
                #         },
                #         {
                #             "field": "RecordTypeId",
                #             "elementReference": "In_Queue_Resource.Id"
                #         },
                #         {
                #             "field": "OwnerId",
                #             "elementReference": "In_RecordType_ResourceDatabase.Id"
                #         },
                #         {
                #             "field": "Service_Name__c",
                #             "elementReference": srvname_field.find(f"name").text
                #         },
                #         {
                #             "field": "Subject",
                #             "elementReference": subject_field.find(f"name").text
                #         },
                #         {
                #             "field": "Support_Team__c",
                #             "stringValue": "Resource Center"
                #         },
                #     ],
                # })

                # fileUp_field = create_tag(type="screen_field", data={
                #     "name": f"Field_FileUpload_when_Lv1_{choice_apiname.replace('Level1Cat_', '').replace('_Choice', '')}",
                #     "fieldType": "ComponentInstance",
                #     "isRequired": "true",
                #     "extensionName": "forceContent:fileUpload",
                #     "inputsOnNextNavToAssocScrn": "UseStoredValues",
                #     "storeOutputAutomatically": "true",
                #     "inputParameters": [
                #         {
                #             "name": "label",
                #             "typeValueTag": "stringValue",
                #             "typeValueText": "Attachments",
                #         },
                #         {
                #             "name": "recordId",
                #             "elementReference": createR_screen.find("name").text
                #         },
                #         {
                #             "name": "multiple",
                #             "typeValueTag": "booleanValue",
                #             "typeValueText": "true",
                #         },
                #         {
                #             "name": "disabled",
                #             "typeValueTag": "booleanValue",
                #             "typeValueText": "false",
                #         },
                #     ]
                # })
                # fileUp_screen = create_tag(type="screens", data={
                #     "name": createR_screen.find("./connector/targetReference").text,
                #     "label": "Attachments (Optional)",
                #     "allowBack": "true",
                #     "allowFinish": "true",
                #     "allowPause": "true",
                #     "showFooter": "true",
                #     "showHeader": "true",
                #     "x": str(colx),
                #     "y": str(rowx+incy*2),
                #     "fields": [
                #         fileUp_field
                #     ]
                # })

                # screen_idx = -1
                # createR_inserted = False
                # for child in root:
                #     screen_idx += 1
                #     if child.tag == f"{ns}processType":
                #         root.insert(screen_idx+1, createR_screen)
                #         break

                # screen_idx = -1
                # detected = False
                # for child in root:
                #     screen_idx += 1
                #     if child.tag == f"recordCreates":
                #         detected = True
                #     if detected and child.tag != f"recordCreates":
                #         root.insert(screen_idx, casedet_screen)
                #         break

                # screen_idx = -1
                # detected = False
                # for child in root:
                #     screen_idx += 1
                #     if child.tag == "screens" and "CaseDetails_when" in child.find("name").text:
                #         detected = True
                #     if detected and (child.tag != "screens" or child.tag == "screens" and "CaseDetails_when" not in child.find("name").text):
                #         root.insert(screen_idx, fileUp_screen)
                #         break

                # if not first_branch_completed and fault_screen is not None:
                #     screen_idx = -1
                #     detected = False
                #     for child in root:
                #         screen_idx += 1
                #         if child.tag == "screens" and "FileUpload_when" in child.find("name").text:
                #             detected = True
                #         if detected and (child.tag != "screens" or child.tag == "screens" and "FileUpload_when" not in child.find("name").text):
                #             root.insert(screen_idx, fault_screen)
                #             break
                
                first_branch_completed = True
                colx += incx*2

            break

    # Remove testing elements
    for decision in tree.findall(f"{ns}decisions"):
        for rule in decision.findall(f"{ns}rules"):
            if rule.find(f"{ns}name").text == "test2":
                decision.remove(rule)
                tree.find(f"./{ns}start/{ns}connector/{ns}targetReference").text = decisions.find(f"{ns}name").text
                break

    save(tree, filename=f"/Users/daniel.m/Git/211-san-diego/force-app/main/default/flows/HelpDesk_Resource_Center.flow-meta.xml")

