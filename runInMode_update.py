import os
import xml.etree.ElementTree as ET

from helpdesk_datarequest import FLOW_LAYOUT
from sal_119 import INCREMENT_X, INCREMENT_Y, namespace, save, xml_printer
from screen_builder import build_flow

if __name__ == '__main__':
    flow_folder = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows'
    flow_files = os.listdir(flow_folder)

    new_runInMode = 'SystemModeWithoutSharing'
    new_description = f"Update runInMode to '{new_runInMode}'."

    for flowfile in flow_files:
        flow_path = f'{flow_folder}/{flowfile}'
        tree = ET.parse(flow_path)
        root = tree.getroot()
        ns = namespace(root)

        # Run In Mode Update
        runInMode = tree.find(f'{ns}runInMode')
        isNew = False
        if runInMode is None:
            isNew = True
            runInMode = ET.Element('runInMode')

        runInMode.text = new_runInMode

        if isNew:
            FLOW_LAYOUT['runInMode'].append(runInMode)


        # Description Update
        description = tree.find(f'{ns}description')
        isNew = False
        if description is None:
            isNew = True
            description = ET.Element('description')

        description.text = new_description

        if isNew:
            FLOW_LAYOUT['description'].append(description)

        tree = build_flow(FLOW_LAYOUT, root)
        save(tree, filename=flow_path)
        FLOW_LAYOUT['runInMode'] = []
        FLOW_LAYOUT['description'] = []
