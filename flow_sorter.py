import functools
import json
import os
from venv import create
from webbrowser import get
import xml.etree.ElementTree as ET
import xml
from typing import List

import yaml

from sal_119 import (FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y, create_tag, get_support_team_name,
                     namespace, save, xml_printer, tagname, deduplicate, tag_sorter)
from screen_builder import build_flow


if __name__ == "__main__":

    flow_folder = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows'

    for file_name in ['HelpDesk_Subflow_Email_Notification.flow-meta.xml']:  # Iterate over flow files
        flow_filename = os.path.join(flow_folder, file_name)
        flow = ET.parse(flow_filename)
        root = flow.getroot()
        ns = namespace(root)

        for child in root:  # Fill the LAYOUT
            tag = tagname(child)
            FLOW_LAYOUT[tag].append(child)
            if 'screens' in child.tag:
                xml_printer(child)

        flow = create_tag(type='base_skeleton', data={
            'label': root.find(f'{ns}label').text,
            'start.locationX': root.find(f'{ns}start/{ns}locationX').text,
            'start.locationY': root.find(f'{ns}start/{ns}locationY').text,
            'start.targetReference': root.find(f'{ns}start/{ns}connector/{ns}targetReference').text,
            'description': root.find(f'{ns}description').text,
        })

        ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")
        save(ET.ElementTree(flow), filename=flow_filename)
        tree = ET.parse(flow_filename)
        flow = tree.getroot()

        xml_printer(flow)

        tree = build_flow(FLOW_LAYOUT, flow)
        # xml_printer(tree.getroot())
        deduplicate(tree)

        save(tree, filename=flow_filename)
