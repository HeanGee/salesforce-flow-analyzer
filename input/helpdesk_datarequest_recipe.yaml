label: HelpDesk - Data Request
start.locationX: 50
start.locationY: 0
start.targetReference: Decision_Level1Category_when_IRAIs_DataRequest

decisions:
  name: Decision_Level1Category_when_IRAIs_DataRequest
  label: If the Level 1 Category is
  defaultConnectorLabel: Default Outcome
  rules:
    - name: Aggregate_or_Summary_Report_of_Client_Data
      label: Aggregate or Summary Report of Client Data
      conditionLogic: and
      leftValueReference: In_Lv1
      operator: EqualTo
      rightStringValue: Aggregate or Summary Report of Client Data
      targetReference: CaseDetails_when_Lv1_AggregateOrSummaryReportOfClientData
    - name: Detail_Level_Client_Data
      label: Detail-Level Client Data
      conditionLogic: and
      leftValueReference: In_Lv1
      operator: EqualTo
      rightStringValue: Detail-Level Client Data
      targetReference: CaseDetails_when_Lv1_DetailLevelClientData
    - name: Resource_Database_Service_Listing
      label: Resource Database Service Listing
      conditionLogic: and
      leftValueReference: In_Lv1
      operator: EqualTo
      rightStringValue: Resource Database Service Listing
      targetReference: CaseDetails_when_Lv1_ResourceDatabaseServiceListing
    - name: CIE_Partner_Utilization_Report
      label: CIE Partner Utilization Report
      conditionLogic: and
      leftValueReference: In_Lv1
      operator: EqualTo
      rightStringValue: CIE Partner Utilization Report
      targetReference: CaseDetails_when_Lv1_CIEPartnerUtilizationReport

dynamicChoiceSets:
  - name: FormatOfDataRequest_ChoiceSet
    dataType: Multipicklist
    displayField-xsi:nil: true
    object-xsi:nil: true
    picklistField: Format_of_Data_Request__c
    picklistObject: Case
  - name: TypeOfUtilizationReport_ChoiceSet
    dataType: Picklist
    displayField-xsi:nil: true
    object-xsi:nil: true
    picklistField: Type_of_Utilization_Report__c
    picklistObject: Case

screens:
  - name: Fault_Screen
    label: Error Message
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    fields:
      - name: Fault_Message
        fieldText: "You have reached an error. Please reach out to your Salesforce administrator with the following error message:\n\n{!$Flow.FaultMessage}"
        fieldType: DisplayText
  
  # Case Details Screens
  - name: CaseDetails_when_Lv1_AggregateOrSummaryReportOfClientData
    label: Case Details
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    targetReference: CreateCase_when_Lv1_AggregateOrSummaryReportOfClientData
    fields:
      - name: Field_Subject
        dataType: String
        fieldText: Subject
        fieldType: InputField
        isRequired: true
      - name: Field_Description
        fieldText: Description
        fieldType: LargeTextArea
        isRequired: true
      - name: Field_DateTimeOfService
        dataType: DateTime
        fieldText: When does the request need to be fulfilled?
        fieldType: InputField
        isRequired: true
      - name: Field_FormatOfDataRequest
        dataType: String
        fieldText: 'Key questions to consider: What is the purpose of the request? What message do you need to or would like to convey? Who is the intended audience or who is the report for?'
        fieldType: MultiSelectPicklist
        isRequired: true
        choiceReferences: FormatOfDataRequest_ChoiceSet
      - name: Field_AdditionalInfo
        fieldText: What other information do you think is needed to resolve this issue?
        fieldType: LargeTextArea
        isRequired: true
  - name: CaseDetails_when_Lv1_DetailLevelClientData
    label: Case Details
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    targetReference: CreateCase_when_Lv1_DetailLevelClientData
    fields:
      - name: Field_Subject
        dataType: String
        fieldText: Subject
        fieldType: InputField
        isRequired: true
      - name: Field_Description
        fieldText: Description
        fieldType: LargeTextArea
        isRequired: true
      - name: Field_DateTimeOfService
        dataType: DateTime
        fieldText: When does the request need to be fulfilled?
        fieldType: InputField
        isRequired: true
      - name: Field_AdditionalInfo
        fieldText: What other information do you think is needed to resolve this issue?
        fieldType: LargeTextArea
        isRequired: true
  - name: CaseDetails_when_Lv1_ResourceDatabaseServiceListing
    label: Case Details
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    targetReference: CreateCase_when_Lv1_ResourceDatabaseServiceListing
    fields:
      - name: Field_Subject
        dataType: String
        fieldText: Subject
        fieldType: InputField
        isRequired: true
      - name: Field_Description
        fieldText: Description
        fieldType: LargeTextArea
        isRequired: true
      - name: Field_DateTimeOfService
        dataType: DateTime
        fieldText: When does the request need to be fulfilled?
        fieldType: InputField
        isRequired: true
      - name: Field_FormatOfDataRequest
        dataType: String
        fieldText: 'Key questions to consider: What is the purpose of the request? What message do you need to or would like to convey? Who is the intended audience or who is the report for?'
        fieldType: MultiSelectPicklist
        isRequired: true
        choiceReferences: FormatOfDataRequest_ChoiceSet
      - name: Field_AdditionalInfo
        fieldText: What other information do you think is needed to resolve this issue?
        fieldType: LargeTextArea
        isRequired: true
  - name: CaseDetails_when_Lv1_CIEPartnerUtilizationReport
    label: Case Details
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    targetReference: CreateCase_when_Lv1_CIEPartnerUtilizationReport
    fields:
      - name: Field_Subject
        dataType: String
        fieldText: Subject
        fieldType: InputField
        isRequired: true
      - name: Field_Description
        fieldText: Description
        fieldType: LargeTextArea
        isRequired: true
      - name: Field_DateTimeOfService
        dataType: DateTime
        fieldText: When does the request need to be fulfilled?
        fieldType: InputField
        isRequired: true
      - name: Field_TypeOfReport
        dataType: String
        fieldText: What type of CIE Partner Utilization Report does this data request concern?
        fieldType: DropdownBox
        isRequired: true
        choiceReferences: TypeOfUtilizationReport_ChoiceSet
      - name: Field_AccountName
        extensionName: flowruntime:lookup
        fieldType: ComponentInstance
        inputsOnNextNavToAssocScrn: UseStoredValues
        storeOutputAutomatically: true
        isRequired: true
        inputParameters:
          - name: fieldApiName
            typeValueTag: stringValue
            typeValueText: AccountId
          - name: label
            typeValueTag: stringValue
            typeValueText: What is the name of the agency?
          - name: objectApiName
            typeValueTag: stringValue
            typeValueText: Case
          - name: required
            typeValueTag: booleanValue
            typeValueText: true
      - name: Field_ServiceName
        dataType: String
        fieldText: What is the name of the service?
        fieldType: InputField
        isRequired: true
      - name: Field_AdditionalInfo
        fieldText: What other information do you think is needed to resolve this issue?
        fieldType: LargeTextArea
        isRequired: true
  
  # File Upload Screens
  - name: FileUpload_when_Lv1_AggregateOrSummaryReportOfClientData
    label: Attachments (Optional)
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    targetReference: FinalizationScreen_when_Lv1_AggregateOrSummaryReportOfClientData
    fields:
      - name: Field_FileUpload_when_Lv1_AggregateOrSummaryReportOfClientData
        fieldType: ComponentInstance
        isRequired: true
        extensionName: forceContent:fileUpload
        inputsOnNextNavToAssocScrn: UseStoredValues
        storeOutputAutomatically: true
        inputParameters: 
          - name: label
            typeValueTag: stringValue
            typeValueText: Attachments
          - name: recordId
            elementReference: CreateCase_when_Lv1_AggregateOrSummaryReportOfClientData
          - name: multiple
            typeValueTag: booleanValue
            typeValueText: true
          - name: disabled
            typeValueTag: booleanValue
            typeValueText: false
  - name: FileUpload_when_Lv1_DetailLevelClientData
    label: Attachments (Optional)
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    targetReference: FinalizationScreen_when_Lv1_DetailLevelClientData
    fields:
      - name: Field_FileUpload_when_Lv1_DetailLevelClientData
        fieldType: ComponentInstance
        isRequired: true
        extensionName: forceContent:fileUpload
        inputsOnNextNavToAssocScrn: UseStoredValues
        storeOutputAutomatically: true
        inputParameters: 
          - name: label
            typeValueTag: stringValue
            typeValueText: Attachments
          - name: recordId
            elementReference: CreateCase_when_Lv1_DetailLevelClientData
          - name: multiple
            typeValueTag: booleanValue
            typeValueText: true
          - name: disabled
            typeValueTag: booleanValue
            typeValueText: false
  - name: FileUpload_when_Lv1_ResourceDatabaseServiceListing
    label: Attachments (Optional)
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    targetReference: FinalizationScreen_when_Lv1_ResourceDatabaseServiceListing
    fields:
      - name: Field_FileUpload_when_Lv1_ResourceDatabaseServiceListing
        fieldType: ComponentInstance
        isRequired: true
        extensionName: forceContent:fileUpload
        inputsOnNextNavToAssocScrn: UseStoredValues
        storeOutputAutomatically: true
        inputParameters: 
          - name: label
            typeValueTag: stringValue
            typeValueText: Attachments
          - name: recordId
            elementReference: CreateCase_when_Lv1_ResourceDatabaseServiceListing
          - name: multiple
            typeValueTag: booleanValue
            typeValueText: true
          - name: disabled
            typeValueTag: booleanValue
            typeValueText: false
  - name: FileUpload_when_Lv1_CIEPartnerUtilizationReport
    label: Attachments (Optional)
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    targetReference: FinalizationScreen_when_Lv1_CIEPartnerUtilizationReport
    fields:
      - name: Field_FileUpload_when_Lv1_CIEPartnerUtilizationReport
        fieldType: ComponentInstance
        isRequired: true
        extensionName: forceContent:fileUpload
        inputsOnNextNavToAssocScrn: UseStoredValues
        storeOutputAutomatically: true
        inputParameters: 
          - name: label
            typeValueTag: stringValue
            typeValueText: Attachments
          - name: recordId
            elementReference: CreateCase_when_Lv1_CIEPartnerUtilizationReport
          - name: multiple
            typeValueTag: booleanValue
            typeValueText: true
          - name: disabled
            typeValueTag: booleanValue
            typeValueText: false

  # Finalization Screen
  - name: FinalizationScreen_when_Lv1_AggregateOrSummaryReportOfClientData
    label: Finalization Screen
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    fields:
      - name: FinalizationMessage
        fieldText: "Completion time for data requests depends on the nature and complexity of your data request. Expect a minimum of 3 business days and up to 6 weeks. In general, please allow for at least 5 business days for basic data requests. If there is a hard deadline, make sure this is clearly stated."
        fieldType: DisplayText
  - name: FinalizationScreen_when_Lv1_DetailLevelClientData
    label: Finalization Screen
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    fields:
      - name: FinalizationMessage
        fieldText: "Completion time for data requests depends on the nature and complexity of your data request. Expect a minimum of 3 business days and up to 6 weeks. In general, please allow for at least 5 business days for basic data requests. If there is a hard deadline, make sure this is clearly stated."
        fieldType: DisplayText
  - name: FinalizationScreen_when_Lv1_ResourceDatabaseServiceListing
    label: Finalization Screen
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    fields:
      - name: FinalizationMessage
        fieldText: "Completion time for data requests depends on the nature and complexity of your data request. Expect a minimum of 3 business days and up to 6 weeks. In general, please allow for at least 5 business days for basic data requests. If there is a hard deadline, make sure this is clearly stated."
        fieldType: DisplayText
  - name: FinalizationScreen_when_Lv1_CIEPartnerUtilizationReport
    label: Finalization Screen
    allowBack: true
    allowFinish: true
    allowPause: true
    showFooter: true
    showHeader: true
    fields:
      - name: FinalizationMessage
        fieldText: "Completion time for data requests depends on the nature and complexity of your data request. Expect a minimum of 3 business days and up to 6 weeks. In general, please allow for at least 5 business days for basic data requests. If there is a hard deadline, make sure this is clearly stated."
        fieldType: DisplayText

recordCreates:
  - name: CreateCase_when_Lv1_AggregateOrSummaryReportOfClientData
    label: Create Case
    object: Case
    targetReference_c: FileUpload
    targetReference_fc: Fault_Screen
    isGoTo_fc: false
    storeOutputAutomatically: true
    inputAssignments:
      - field: Additional_Information__c
        elementReference: Field_AdditionalInfo
      - field: Description
        elementReference: Field_Description
      - field: Initial_Request_Area__c
        elementReference: In_InitialRequestArea
      - field: Level_1_Category__c
        elementReference: In_Lv1
      - field: RecordTypeId
        elementReference: In_RecordType_DataRequest.Id
      - field: OwnerId
        elementReference: In_Queue_Data.Id
      - field: Subject
        elementReference: Field_Subject
      - field: Support_Team__c
        stringValue: Informatics
      - field: Date_and_Time_of_Service__c
        elementReference: Field_DateTimeOfService
      - field: Format_of_Data_Request__c
        elementReference: Field_FormatOfDataRequest
  - name: CreateCase_when_Lv1_DetailLevelClientData
    label: Create Case
    object: Case
    targetReference_c: FileUpload
    targetReference_fc: Fault_Screen
    isGoTo_fc: false
    storeOutputAutomatically: true
    inputAssignments:
      - field: Additional_Information__c
        elementReference: Field_AdditionalInfo
      - field: Description
        elementReference: Field_Description
      - field: Initial_Request_Area__c
        elementReference: In_InitialRequestArea
      - field: Level_1_Category__c
        elementReference: In_Lv1
      - field: RecordTypeId
        elementReference: In_RecordType_DataRequest.Id
      - field: OwnerId
        elementReference: In_Queue_Data.Id
      - field: Subject
        elementReference: Field_Subject
      - field: Support_Team__c
        stringValue: Informatics
      - field: Date_and_Time_of_Service__c
        elementReference: Field_DateTimeOfService
  - name: CreateCase_when_Lv1_ResourceDatabaseServiceListing
    label: Create Case
    object: Case
    targetReference_c: FileUpload
    targetReference_fc: Fault_Screen
    isGoTo_fc: false
    storeOutputAutomatically: true
    inputAssignments:
      - field: Additional_Information__c
        elementReference: Field_AdditionalInfo
      - field: Description
        elementReference: Field_Description
      - field: Initial_Request_Area__c
        elementReference: In_InitialRequestArea
      - field: Level_1_Category__c
        elementReference: In_Lv1
      - field: RecordTypeId
        elementReference: In_RecordType_DataRequest.Id
      - field: OwnerId
        elementReference: In_Queue_Data.Id
      - field: Subject
        elementReference: Field_Subject
      - field: Support_Team__c
        stringValue: Informatics
      - field: Date_and_Time_of_Service__c
        elementReference: Field_DateTimeOfService
  - name: CreateCase_when_Lv1_CIEPartnerUtilizationReport
    label: Create Case
    object: Case
    targetReference_c: FileUpload
    targetReference_fc: Fault_Screen
    isGoTo_fc: false
    storeOutputAutomatically: true
    inputAssignments:
      - field: Additional_Information__c
        elementReference: Field_AdditionalInfo
      - field: Description
        elementReference: Field_Description
      - field: Initial_Request_Area__c
        elementReference: In_InitialRequestArea
      - field: Level_1_Category__c
        elementReference: In_Lv1
      - field: RecordTypeId
        elementReference: In_RecordType_DataRequest.Id
      - field: OwnerId
        elementReference: In_Queue_CIELocal.Id
      - field: Subject
        elementReference: Field_Subject
      - field: Support_Team__c
        stringValue: CIE Local
      - field: Date_and_Time_of_Service__c
        elementReference: Field_DateTimeOfService
      - field: Type_of_Utilization_Report__c
        elementReference: Field_TypeOfReport
      - field: AccountId
        elementReference: Field_AccountName
        isIDField: true
      - field: Service_Name__c
        elementReference: Field_ServiceName

variables:
  - name: In_Lv1
    dataType: String
    isCollection: false
    isInput: true
    isOutput: false
  - name: In_InitialRequestArea
    dataType: String
    isCollection: false
    isInput: true
    isOutput: false
  - name: In_RecordType_ResourceDatabase
    dataType: SObject
    isCollection: false
    isInput: true
    isOutput: false
    objectType: RecordType
  - name: In_RecordType_DataRequest
    dataType: SObject
    isCollection: false
    isInput: true
    isOutput: false
    objectType: RecordType
  - name: In_Queue_Resource
    dataType: SObject
    isCollection: false
    isInput: true
    isOutput: false
    objectType: Group
  - name: In_Queue_Data
    dataType: SObject
    isCollection: false
    isInput: true
    isOutput: false
    objectType: Group
  - name: In_Queue_CIELocal
    dataType: SObject
    isCollection: false
    isInput: true
    isOutput: false
    objectType: Group
