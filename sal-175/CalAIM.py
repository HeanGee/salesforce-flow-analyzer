import a_sectionizer
import b_fields_yaml_generator
import c_screen_generator


if __name__ == '__main__':
    a_sectionizer.run()
    b_fields_yaml_generator.run()
    c_screen_generator.run()