import json
import os
import re

import pandas as pd
import yaml as ym


def __rename_datatype(dataType: str):
    renaming_map = {
        'Long text area': 'LargeTextArea',
        'Lookup(User)': 'Lookup',
        'Lookup (Screening)': 'Lookup',
        'Lookup (Account)': 'Lookup',
        'Multi-select picklist': 'MultiSelectPicklist',
        'Date/Time': 'DateTime',
    }

    return renaming_map[dataType] if dataType in renaming_map.keys() else dataType


def run():
    """
    Generates one yaml file for each section with the corresponding fields.
    The input file is the final_fields.csv.
    """

    output_directory = 'sal-175/fields_by_screens'

    # Clear output directory
    for file in os.listdir(output_directory):
        os.remove(os.path.join(output_directory, file))

    inputfilepath = 'sal-175/final_fields.csv'
    finalfields = pd.read_csv(inputfilepath, usecols=['New Field Label', 'New API Name', 'Data Type', 'SECTION'])
    finalfields.fillna('', inplace=True)

    current_section = None
    yaml_body = []
    skipped_fields = []

    for _, row in finalfields.iterrows():
        section = row['SECTION'].strip()
        label = row['New Field Label'].strip()
        field_apiname = row['New API Name'].strip()

        if section == '':
            skipped_fields.append({'field': label, 'section': section, 'reason': 'Field does not have section'})
            continue
        elif section == 'Screening Questions':
            skipped_fields.append({'field': label, 'section': section, 'reason': 'The field corresponds to Screening Object. The field will be displayed in the page layout and with a value only if the intake record has an associated screening.'})
            continue
        elif field_apiname in ['Chronic_Health_Ed_Resources_Offered__c']:
            skipped_fields.append({'field': label, 'section': section, 'reason': 'The field does not exist in the Intake Object'})
            continue

        if section != current_section:
            if yaml_body is not None and current_section is not None:
                output_yamlfilepath = f'{output_directory}/Fields_{"".join(current_section.split(" "))}.yaml'
                with open(output_yamlfilepath, 'w') as file:
                    documents = ym.dump(yaml_body, file)
                yaml_body = []
            current_section = section
            
        if section == current_section:
            dataType = row['Data Type']

            renamedDataType = __rename_datatype(dataType)
            yaml_data = {
                'label': label,
                'dataType': renamedDataType,
                'isRequired': 'false',
                'field_apiname': field_apiname
            }

            # Parse Lookup element
            # pattern = r'[A-Z|a-z]*[ ]*\(([A-Z|a-z]*)\)'
            # match = re.match(pattern, dataType)
            
            # if match is not None:
            #     sobject_apiname = match[1].strip()

            #     if field_apiname == "":
            #         skipped_fields.append({'field': label, 'section': section, 'apiname': field_apiname, 'datatype': dataType, 'reason': 'Field does not have the corresponding API name'})
            #         continue

            yaml_data['object'] = 'Intake__c'
            yaml_data['fieldName'] = field_apiname

            # Construct yaml_body
            yaml_body.append(yaml_data)

    # Save collected fields that weren't processed due to for loop
    if yaml_body is not None and current_section is not None:
        output_yamlfilepath = f'{output_directory}/Fields_{"".join(current_section.split(" "))}.yaml'
        with open(output_yamlfilepath, 'w') as file:
            documents = ym.dump(yaml_body, file)
        yaml_body = []

    # Dump to file if skipped fields
    if skipped_fields is not None:
        with open(f'{output_directory}/Skipped_fields.json', 'w') as file:
            json.dump(skipped_fields, file, indent=2)


if __name__ == '__main__':
    run()