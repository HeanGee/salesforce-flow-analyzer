import configparser as cfg
import json
import os
import re
import sys
import xml.etree.ElementTree as ET
from copy import deepcopy
from doctest import debug

import yaml

try:
    # getting the name of the directory
    # where the this file is present.
    current = os.path.dirname(os.path.realpath(__file__))

    # Getting the parent directory name
    # where the current directory is present.
    parent = os.path.dirname(current)

    # adding the parent directory to
    # the sys.path.
    sys.path.append(parent)

    from sal_119 import FLOW_LAYOUT, build_regions, create_tag, namespace, save, xml_printer
    from screen_builder import build_flow, fill_flow
except Exception as exc:
    raise exc


__ELEMENT_TEMPLATE__ = {
    'Date': {
        'name': '',
        'dataType': 'Date',
        'fieldText': '',
        'fieldType': 'InputField',
        'isRequired': 'false',
    },
    'DateTime': {
        'name': '',
        'dataType': 'Date',
        'fieldText': '',
        'fieldType': 'InputField',
        'isRequired': 'false',
    },
    'Text': {
        'name': '',
        'dataType': 'String',
        'fieldText': '',
        'fieldType': 'InputField',
        'isRequired': 'false',
    },
    'String': {
        'name': '',
        'dataType': 'String',
        'fieldText': '',
        'fieldType': 'InputField',
        'isRequired': 'false',
    },
    'MultiSelectPicklist': {
        'name': '',
        'choiceReferences': '',
        'dataType': 'String',
        'fieldText': '',
        'fieldType': 'MultiSelectPicklist',
        'isRequired': 'false',
    },
    'Picklist': {
        'name': '',
        'choiceReferences': '',
        'dataType': 'String',
        'fieldText': '',
        'fieldType': 'DropdownBox',
        'isRequired': 'false',
        'type': 'Record Choice Set',
    },
    'Lookup': {
        'name': '',
        'extensionName': 'flowruntime:lookup',
        'fieldType': 'ComponentInstance',
        'inputParameters': [
            {
                'name': 'fieldApiName',
                'typeValueTag': 'stringValue',
                'typeValueText': '',
            },
            {
                'name': 'label',
                'typeValueTag': 'stringValue',
                'typeValueText': '',
            },
            {
                'name': 'objectApiName',
                'typeValueTag': 'stringValue',
                'typeValueText': '',
            },
            {
                'name': 'required',
                'typeValueTag': 'booleanValue',
                'typeValueText': 'false',
            },
        ],
        'inputsOnNextNavToAssocScrn': 'UseStoredValues',
        'isRequired': 'false',
        'storeOutputAutomatically': 'true',

    },
    'Phone': {
        'name': '',
        'extensionName': 'flowruntime:phone',
        'fieldType': 'ComponentInstance',
        'inputParameters': [
            {
                'name': 'label',
                'typeValueTag': 'stringValue',
                'typeValueText': '',
            },
            {
                'name': 'required',
                'typeValueTag': 'booleanValue',
                'typeValueText': 'false',
            },
        ],
        'inputsOnNextNavToAssocScrn': 'UseStoredValues',
        'isRequired': 'false',
        'storeOutputAutomatically': 'true',
    },
    'Checkbox': {
        'name': '',
        'dataType': 'Boolean',
        'fieldText': '',
        'fieldType': 'InputField',
        'isRequired': 'false',
    },
    'Boolean': {
        'name': '',
        'dataType': 'Boolean',
        'fieldText': '',
        'fieldType': 'InputField',
        'isRequired': 'false',
    },
    'LargeTextArea': {
        'name': '',
        'fieldText': '',
        'fieldType': 'LargeTextArea',
        'isRequired': 'false',
    },
    'Text Area': {
        'name': '',
        'dataType': 'String',
        'fieldText': '',
        'fieldType': 'InputField',
        'isRequired': 'false',
    },
    'TextArea': {
        'name': '',
        'dataType': 'String',
        'fieldText': '',
        'fieldType': 'InputField',
        'isRequired': 'false',
    },
}


def field_parser(yaml_filepath: str):
    recipe = yaml.safe_load(open(yaml_filepath, 'r'))
    fields = []
    dynamicChoiceSets = []
    inputAssignments_for_RecordCreate = []

    for field in recipe:
        label = field['label'].strip()
        dataType = field['dataType'].strip()
        isRequired = field['isRequired'].strip().lower()
        field_apiname = field['field_apiname'].strip()

        element_template = deepcopy(__ELEMENT_TEMPLATE__[dataType])
        fieldname_without_space = ''.join(label.replace('-', '').replace('/', '').split(' '))
        element_template['name'] = f'Field_{fieldname_without_space}'

        # Collect inputAssignments_for_RecordCreate
        inputAssignments_for_RecordCreate.append({'field': field_apiname, 'elementReference': element_template['name'] + ('.recordId' if dataType == 'Lookup' else '')})

        if dataType in ['Date', 'DateTime', 'Text', 'String', 'Checkbox', 'Boolean', 'Text Area', 'TextArea']:
            element_template['fieldText'] = label
            element_template['isRequired'] = isRequired if isRequired is not None else element_template['isRequired']

        elif dataType in ['MultiSelectPicklist', 'Picklist']:
            element_template['fieldText'] = label
            element_template['isRequired'] = isRequired if isRequired is not None else element_template['isRequired']

            # Generate choiceReferences for Picklist fields. The SObject is Intake
            # if dataType == 'Picklist' and 'type' in element_template.keys() and element_template['type'] == 'Record Choice Set':
            # Collect dynamicChoiceElement
            choiceSetName = f'PicklistChoiceSet_{fieldname_without_space}'.replace('__c', '')
            element_template['choiceReferences'] = choiceSetName
            dynamicChoiceElement = create_tag(element_type='dynamicChoiceSets', data={
                "name": choiceSetName,
                "dataType": 'Picklist' if dataType == 'Picklist' else 'Multipicklist',

                "displayField-xsi:nil": 'true',
                "object-xsi:nil": 'true',
                "picklistField": field_apiname,
                "picklistObject": 'Intake__c',
            })

            dynamicChoiceSets.append(dynamicChoiceElement)

        elif dataType == 'Lookup':
            element_template['isRequired'] = isRequired if isRequired is not None else element_template['isRequired']

            for ip in element_template['inputParameters']:
                if ip['name'] == 'fieldApiName':
                    ip['typeValueTag'] = 'stringValue'
                    ip['typeValueText'] = field['fieldName']

                elif ip['name'] == 'label':
                    ip['typeValueTag'] = 'stringValue'
                    ip['typeValueText'] = label

                elif ip['name'] == 'objectApiName':
                    ip['typeValueTag'] = 'stringValue'
                    ip['typeValueText'] = field['object']

                elif ip['name'] == 'required':
                    ip['typeValueTag'] = 'booleanValue'
                    ip['typeValueText'] = isRequired if isRequired is not None else ip['typeValueText']

        elif dataType == 'LargeTextArea':
            element_template['fieldText'] = label
            element_template['fieldType'] = 'LargeTextArea'
            element_template['isRequired'] = isRequired if isRequired is not None else element_template['isRequired']

        elif dataType == 'Phone':
            element_template['isRequired'] = isRequired if isRequired is not None else element_template['isRequired']

            for ip in element_template['inputParameters']:
                if ip['name'] == 'label':
                    ip['typeValueTag'] = 'stringValue'
                    ip['typeValueText'] = label

                elif ip['name'] == 'required':
                    ip['typeValueTag'] = 'booleanValue'
                    ip['typeValueText'] = isRequired if isRequired is not None else ip['typeValueText']

        field = create_tag('screen_field', data=element_template)
        if field is None:
            print(dataType, 'returned none')
            print(json.dumps(element_template, indent=2))
            print()
        fields.append(field)

    return fields, dynamicChoiceSets, inputAssignments_for_RecordCreate


def camel_case_split(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]


def run():
    try:
        # Create base flow
        flow, FLOW_LAYOUT = fill_flow("sal-175/sal-175.yaml")

        input_directory = 'sal-175/fields_by_screens'

        # Get only yaml files
        fieldlist_yamlfiles = filter(lambda filepath: '.yaml' in filepath, os.listdir(input_directory))

        inputAssignments_for_RecordCreate = []
        for fields_yamlfile in fieldlist_yamlfiles:
            # debugging
            # if fields_yamlfile == 'Fields_ClientInformation.yaml':
            #     continue 

            # Load fields file and create ET Elements. Current file corresponds to one section
            section = re.match(r'[a-z|A-Z]*_([a-z|A-Z]*)', fields_yamlfile)[1]
            filepath = f'{input_directory}/{fields_yamlfile}'

            print(f'Processing {filepath}')
            print('======================')

            # Parse and create fields elements.
            # Also collect dynamicChoiceElement and inputAssignments_for_RecordCreate
            fieldlist, dynamicChoiceSets, inputAssignments = field_parser(filepath)

            # Collect fields in array
            fields = ET.Element('fields')
            {fields.append(field) for field in fieldlist}

            # Distribute fields in N columns
            amount_of_fields = len(fields)
            columns_count = 1 # if amount_of_fields < 5 else 2 if amount_of_fields < 9 else 3 if amount_of_fields < 20 else 4
            region_column_name=f'ScreenSection_{section}_Column_'
            regions = build_regions(field_tags_list=fields, number_of_columns=columns_count, region_column_name=region_column_name)                

            # Build screen section element
            screen_section = create_tag(element_type='screen_field', data={
                'name': f'ScreenSection_{section}',
                'fieldText': ' '.join(camel_case_split(section)),
                'fieldType': 'RegionContainer',
                'regions': regions,
                'isRequired': 'false',
                'regionContainerType': 'SectionWithHeader',
            })

            # Create Screen for the current section
            screens = FLOW_LAYOUT['screens']
            screen_data = {
                'name': f'Screen_{section}',
                'label': ' '.join(camel_case_split(section)),
                'allowBack': 'true',
                'allowFinish': 'true',
                'allowPause': 'true',
                'showFooter': 'true',
                'showHeader': 'true',
                'fields': [screen_section],
                'x': '0',
                'y': '0',
                'targetReference': 'dummy-target',
            }

            screen = create_tag(element_type='screens', data=screen_data)
            screens.append(screen)

            # Create dynamicChoiceSets elements based on the picklist fields
            _ = [FLOW_LAYOUT['dynamicChoiceSets'].append(choiceSet) for choiceSet in dynamicChoiceSets]

            # Collect inputAssignments_for_RecordCreate
            inputAssignments_for_RecordCreate += inputAssignments

        # Link Screens in ORDER
        section_order = json.load(open('sal-175/section_order.json'))
        ordered_screenlist = []

        # Collect screen elements into a list, in the order it should appear indicated in the section_order
        for section, order in section_order.items():
            for screen in screens:
                screen_name = screen.find(f'name').text
                detected_screen = ''.join(section.split(' ')) in screen_name and screen_name not in ['Screen_SelectDomain', 'Screen_EndingMessage']
                
                if detected_screen:
                    ordered_screenlist.append(screen)
                    break
        # Update the connector/targetReference element
        initial_element = FLOW_LAYOUT['assignments'][0]
        last_screen = None
        first_screen = None
        for screen in ordered_screenlist:
            if first_screen is None:
                first_screen = screen

            initial_element.find('connector/targetReference').text = screen.find('name').text
            last_screen = screen
            initial_element = screen
        
        # For last screen, do not create connector --> RecordCreate node now is added so it should have target.
        # last_screen.remove(last_screen.find('connector'))

        FLOW_LAYOUT['screens'] = screens

        # Create recordCreate element
        inputAssignments_for_RecordCreate.append({'field': 'Domain__c', 'elementReference': 'InputVar_Domain'})
        inputAssignments_for_RecordCreate.append({'field': 'Account__c', 'elementReference': 'recordId'})
        rc_data = {
            'name': 'RecordCreate_IntakeRecord',
            'label': 'Create Intake Record',
            'object': 'Intake__c',
            'inputAssignments': inputAssignments_for_RecordCreate,
            'storeOutputAutomatically': 'true',
            'x': 0,
            'y': 0,
            'targetReference_fc': 'Fault_Screen',
            'targetReference_c': 'dummy-target'
        }
        # rc_data['targetReference_c'] = 'dummy-targetReference'
        FLOW_LAYOUT['recordCreates'].append(create_tag(element_type='recordCreates', data=rc_data))

        # For last screen, target it to the recordCreate
        # For RecordCreate, target it to Finalization message screen
        last_screen.find('connector/targetReference').text = 'RecordCreate_IntakeRecord'
        FLOW_LAYOUT['recordCreates'][0].find('connector/targetReference').text = 'Screen_EndingMessage'

        # Build xml tree
        tree = build_flow(FLOW_LAYOUT, flow)
        root = tree.getroot()
        ns = namespace(root)

        # Link Decision branches to the first screen
        assignment = flow.find(f'{ns}assignments')
        default_branch = flow.find(f'{ns}decisions/{ns}defaultConnector/{ns}targetReference')
        
        assignment.find(f'{ns}connector/{ns}targetReference').text = first_screen.find(f'{ns}name').text
        default_branch.text = first_screen.find(f'{ns}name').text

        # Save to Flow file
        input_directory = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows'
        # input_directory = '/Users/daniel.m/Git/211-san-diego/force-app/main/default/flows'
        save(tree, filename=f"{input_directory}/CalAIM_Intake.flow-meta.xml", short_empty_elements=True)

        print('\nDONE\n')
    except Exception as exc:
        raise exc



if __name__ == '__name__':
    run()