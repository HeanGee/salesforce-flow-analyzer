import json
from math import isnan

import pandas as pd


def run():
    page_layout = pd.read_csv('sal-175/pagelayout.csv', index_col=None, usecols=['SECTION', 'FIELD1', 'FIELD2'])
    page_layout = page_layout[(~page_layout['FIELD1'].isna()) & (~page_layout['FIELD1'].isnull())]
    page_layout.set_index('SECTION', inplace=True)
    page_layout.index = pd.Series(page_layout.index).fillna(method='ffill')
    page_layout.reset_index(inplace=True)
    page_layout.fillna('', inplace=True)

    fields_by_section = {}
    section_order = {}
    counter = 1
    for _, row in page_layout.iterrows():
        section = row['SECTION'].strip()

        if section not in section_order.keys():
            section_order[section] = counter
            counter += 1

        if section not in fields_by_section:
            fields_by_section[section] = []

        fields_by_section[section].append(row['FIELD1'])

        if row['FIELD2'] != "":
            fields_by_section[section].append(row['FIELD2'])

    # print(json.dumps(fields_by_section, indent=2))
    # page_layout.to_csv('sal-175/new_pagelayout.csv', index=False)
    json.dump(section_order, open('sal-175/section_order.json', 'w'), indent=2)


    dfs = None
    for section in fields_by_section:
        field_count = len(fields_by_section[section])
        series = pd.Series([section])
        df_sections = pd.DataFrame(data={
            'SECTION': series.repeat(field_count)
        }).reset_index(drop=True)

        df_fields = pd.DataFrame(data={
            'FIELD': fields_by_section[section]
        })

        # print(df_sections)
        # print(df_fields)

        df = pd.concat([df_sections, df_fields], axis=1)

        dfs = df if dfs is None else pd.concat([dfs, df])

    dfs.to_csv('sal-175/final-pagelayouts.csv', index=False)


    fields = pd.read_csv('sal-175/Fields.csv', usecols=['New Field Label', 'New API Name', 'Data Type', 'SUM'])
    fields = fields[fields['SUM'] > 0]
    print(len(fields.index))

    big = fields.merge(dfs, how='left', left_on='New Field Label', right_on='FIELD')
    big.insert(loc=len(big.columns), column='ORDER', value=pd.Series(range(1, len(big.index) + 1 ,1)))
    big.sort_values(by=['SECTION', 'ORDER'], inplace=True)
    big.drop(['SUM', 'FIELD'], axis=1, inplace=True)
    big.to_csv('sal-175/final_fields.csv', index=False)
    big.to_excel("sal-175/final_fields.xlsx", index=False)


if __name__ == '__main__':
    run()