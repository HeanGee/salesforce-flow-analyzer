import os
import xml.etree.ElementTree as ET
from argparse import FileType


def dynamicChoiceSets(data: dict = {}):
    """
    Creates the a 'dynamicChoiceSets' xml tag element.

    Args:
        data (dict): A dictionary of parameters required for this specific tag.
            The required parameters are:

            - name (str): The API name for the tag element.
            - picklistField (str): The SObject Picklist Field API name.
            - picklistObject (str): The SObject API name that holds the picklist field.

    Returns:
        dynamicChoiceSets (ET.Element): The xml element of the type 'dynamicChoiceSets'.
    """
    if data is None:
        raise ValueError('xml_tag_creator.dynamicChoiceSets(): missing "data" parameter')

    dynamicChoiceSets = ET.Element('dynamicChoiceSets')

    name = ET.Element('name')
    dataType = ET.Element('dataType')
    displayField = ET.Element('displayField')
    object = ET.Element('object')
    picklistField = ET.Element('picklistField')
    picklistObject = ET.Element('picklistObject')

    name.text = data['name']
    dataType.text = 'Picklist'
    displayField.set('xsi:nil', 'true')
    object.set('xsi:nil', 'true')
    picklistField.text = data['picklistField']
    picklistObject.text = data['picklistObject']

    {dynamicChoiceSets.append(element) for element in [name, dataType, displayField, object, picklistField, picklistObject]}

    return dynamicChoiceSets

def field_DropdownBox(data: dict = {}):
    """
    Creates a screen field of the type 'DropdownBox'.

    Args:
        data (dict): A dictionary of parameters required for this specific tag.
            The required parameters are:

            - name (str): The API name of the element.
            - choiceReferences (str): The API name of the choiceSet element.
            - dataType (str): The type of the value of each choice selection.
            - stringValue (str): The default value when data type of the field is string. Optional.
            - booleanValue (str): The default value when data type of the field is boolean. Optional.
            - fieldText (str): The label of the field.
            - isRequired (str): Whether the user input is required or not.

    Returns:
        field (ET.Element): The xml element of the type 'field:DropdownBox'.
    """

    field = ET.Element('fields')

    name = ET.Element('name')
    choiceReferences = ET.Element('choiceReferences')
    dataType = ET.Element('dataType')
    stringValue = ET.Element('stringValue') if 'stringValue' in data.keys() and data['stringValue'] is not None else None
    # Or booleanValue, integerValue, etc goes here...
    defaultValue = ET.Element('defaultValue') if any([element is not None for element in [stringValue]]) else None
    fieldText = ET.Element('fieldText')
    fieldType = ET.Element('fieldType')
    isRequired = ET.Element('isRequired')

    name.text = data['name']
    choiceReferences.text = data['choiceReferences']
    dataType.text = data['dataType']
    if stringValue is not None:
        stringValue.text = data['stringValue']
        defaultValue.append(stringValue)
    fieldText.text = data['fieldText']
    fieldType.text = 'DropdownBox'
    isRequired.text = data['isRequired']

    {field.append(element) for element in [name, choiceReferences, dataType, defaultValue, fieldText, fieldType, isRequired] if element is not None}

    return field

def recordCreate_inputAssignments(data: dict = {}):
    """
    Creates a screen field of the type 'DropdownBox'.

    Args:
        data (dict): A dictionary of parameters required for this specific tag.
            The required parameters are:

            - field (str): The API name of the SObject field.
            - elementReference (str): The API name of the referenced element. Optional.
            - stringValue (str): The value of the element when the type of data is String. Optional.
            - booleanValue (str): The value of the element when the type of data is Boolean. Optional.

    Returns:
        inputAssignments (ET.Element): The xml element of the type 'recordCreate:inputAssignments'.
    """

    inputAssignments = ET.Element('inputAssignments')

    field = ET.Element('field')
    value = ET.Element('value')
    elementReference = ET.Element('elementReference') if 'elementReference' in data.keys() and data['elementReference'] is not None else None
    stringValue = ET.Element('stringValue') if 'stringValue' in data.keys() and data['stringValue'] is not None else None

    field.text = data['field']
    if elementReference is not None:
        elementReference.text = data['elementReference']
    if stringValue is not None:
        stringValue.text = data['stringValue']
    
    {value.append(element) for element in [elementReference, stringValue] if element is not None}

    {inputAssignments.append(element) for element in [field, value]}

    return inputAssignments