import json
import os
from venv import create
import xml.etree.ElementTree as ET
import xml

import yaml

from sal_119 import (FLOW_LAYOUT, INCREMENT_X, INCREMENT_Y, create_tag,
                     namespace, save, xml_printer)
from screen_builder import build_flow

ET.register_namespace("", "http://soap.sforce.com/2006/04/metadata")


if __name__ == "__main__":

    flow_to_update = 'D:/Users/HeanG/git-repos/211-san-diego/force-app/main/default/flows/HelpDesk_Resource_Center.flow-meta.xml'
    support_team_email = 'resourcecenter@211sandiego.org '
    flowName = 'HelpDesk_Subflow_Email_Notification'
    parent_node_prefix = 'FileUpload_when_'
    email_subflow_prefix = 'Subflow_EmailNotif_when_'
    support_team_name = 'Resource Center'

    tree = ET.parse(flow_to_update)
    root = tree.getroot()
    ns = namespace(root)
    tree.find(f'./{ns}status').text = 'Draft'
    tree.find(f'./{ns}description').text = 'Add missing Finalization Message nodes'

    screens = tree.findall(f'./{ns}screens')
    fileUploads = filter(lambda screen: parent_node_prefix in screen.find(f'./{ns}name').text, screens)

    for fileup in fileUploads:
        current_when = fileup.find(f'./{ns}name').text.replace(parent_node_prefix, '')

        data = {
            'name': f'Finalization_when_' + current_when,
            'label': 'Finalization',
            'x': '1',
            'y': '1',
            'allowBack': 'true',
            'allowFinish': 'true',
            'allowPause': 'true',
            'showFooter': 'true',
            'showHeader': 'true',
            'fields': [
                create_tag(type='screen_field', data={
                    'name': 'Field_Finalization_when_' + current_when,
                    'fieldText': '',
                    'fieldType': 'DisplayText'
                })
            ]
        }
        connector = ET.Element('connector')
        targetReference = ET.Element('targetReference')
        targetReference.text = data['name']
        connector.append(targetReference)
        fileup.append(connector)
        FLOW_LAYOUT['screens'].append(create_tag(type='screens', data=data))

    # Build xml tree
    tree = build_flow(FLOW_LAYOUT, root)
    root = tree.getroot()
    ns = namespace(root)

    save(tree, filename=flow_to_update)